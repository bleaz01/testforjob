// All classes are the same file because it's a small project

class Item {
  constructor(name, sellIn, quality) {
    this.name = name;
    this.sellIn = sellIn;
    this.quality = quality;
  }
}
/**
 * Abstract Class Shop.
 *
 * @class Shop
 */
class Shop {
  constructor(items = []) {
    this.items = items;

    if (this.constructor === Shop) {
      throw new Error("Abstract class Shop can't be instantiated.");
    }
  }

  dayAfterDay() {
    throw new Error("Method 'dayAfterDay()' must be implemented.");
  }

  static updateQuality(items) {
    for (const item of items) {
      if (item.name.includes("Aged Brie"))
        new AgedBrieProduct(item).dayAfterDay();
      else if (item.name.includes("Sulfuras"))
        new LegendaryProduct(item).dayAfterDay();
      else if (item.name.includes("Backstage passes"))
        new BackstageProduct(item).dayAfterDay();
      else if (item.name.includes("Conjured"))
        new ConjuredProduct(item).dayAfterDay();
      else new ManageQualityProduct(item).dayAfterDay();
    }
  }
}

class ManageQualityProduct extends Shop {
  constructor(items) {
    super(items);
    this.MIN_QUALITY = 0;
    this.MAX_QUALITY = 50;
  }

  checkQuality(
    product,
    minQuality = this.MIN_QUALITY,
    maxQuality = this.MAX_QUALITY
  ) {
    product.quality = Math.max(
      minQuality,
      Math.min(product.quality, maxQuality)
    );
    return product;
  }

  handleDegradeQualityProduct(product) {
    if (!product.name.includes("Conjured") && product.sellIn > 0)
      return product.quality--;
    else return (product.quality -= 2);
  }

  handleQualityProduct(product) {
    if (product.name.includes("Sulfuras")) return product.quality;
    if (
      product.name.includes("Aged Brie") ||
      product.name.includes("Backstage passe")
    )
      return product.quality;
    else this.handleDegradeQualityProduct(product);
  }

  dayAfterDay() {
    this.items.sellIn--;
    this.handleQualityProduct.bind(this)(this.items);
    this.checkQuality.bind(this)(this.items);
  }
}
class AgedBrieProduct extends ManageQualityProduct {
  constructor(items) {
    super(items);
  }
  dayAfterDay() {
    this.items.quality++;
    super.checkQuality.bind(this)(this.items);
    super.dayAfterDay();
  }
}
class BackstageProduct extends ManageQualityProduct {
  constructor(items) {
    super(items);
  }
  _checkSellIn(product) {
    if (product.sellIn < 0) product.quality = 0;
    else if (product.sellIn < 5) product.quality += 3;
    else if (product.sellIn < 10) product.quality += 2;
    else product.quality++;
  }

  dayAfterDay() {
    super.dayAfterDay();
    this._checkSellIn.bind(this)(this.items);
    super.checkQuality.bind(this)(this.items);
  }
}
class LegendaryProduct extends ManageQualityProduct {
  constructor(items) {
    super(items);
  }

  checkQuality(product) {
    return super.checkQuality(product, 0, 80);
  }

  dayAfterDay() {
    super.dayAfterDay();
    this.checkQuality.bind(this)(this.items);
    if (this.items.sellIn < 0) this.items.sellIn = 0;
  }
}
class ConjuredProduct extends ManageQualityProduct {
  constructor(items) {
    super(items);
  }
  _twiceFasterDegreated(product) {
    if (product.sellIn < 0) product.quality -= 2;
  }
  dayAfterDay() {
    this._twiceFasterDegreated.bind(this)(this.items);
    super.dayAfterDay();
  }
}
module.exports = {
  Item,
  Shop,
  ManageQualityProduct,
  AgedBrieProduct,
  BackstageProduct,
  ConjuredProduct,
  LegendaryProduct,
};
