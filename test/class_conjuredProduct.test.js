const { expect } = require("chai");
const sinon = require("sinon");

const { Item, ConjuredProduct } = require("../src/gilded_rose.js");

describe("Class ConjuredProduct extend Product ", function () {
  // before(function () {
  const mockInstance = new ConjuredProduct();

  it("should inherited method checkQuality", () => {
    expect(mockInstance.checkQuality).to.exist;
  });
  it("should inherited method handleDegradeQualityProduct", () => {
    expect(mockInstance.handleDegradeQualityProduct).to.exist;
  });

  it("should inherited method handleQualityProduct", () => {
    expect(mockInstance.handleQualityProduct).to.exist;
  });

  it("should inherited method dayAfterDay", () => {
    expect(mockInstance.dayAfterDay).to.exist;
  });

  it("should return success method twiceFasterDegreated", () => {
    expect(mockInstance._twiceFasterDegreated).to.exist;
  });
  describe("method return with success ", () => {
    it("should return method twiceFasterDegreated => if sellIn < 0 return quality -=2", () => {
      let mockProduct = new Item("ConjuredProduct", -1, 6);
      let qualityResult = mockProduct.quality;
      if (mockProduct.sellIn < 0) qualityResult -= 2;
      mockInstance._twiceFasterDegreated(mockProduct);
      expect(mockProduct.quality).to.equal(qualityResult);
    });
  });
});
