const { expect } = require("chai");
const sinon = require("sinon");

const { Item, BackstageProduct } = require("../src/gilded_rose.js");

describe("Class BackstageProduct extend Product ", function () {
  // before(function () {
  const mockInstance = new BackstageProduct();

  it("should inherited method checkQuality", () => {
    expect(mockInstance.checkQuality).to.exist;
  });
  it("should inherited method handleDegradeQualityProduct", () => {
    expect(mockInstance.handleDegradeQualityProduct).to.exist;
  });

  it("should inherited method handleQualityProduct", () => {
    expect(mockInstance.handleQualityProduct).to.exist;
  });

  it("should inherited method dayAfterDay", () => {
    expect(mockInstance.dayAfterDay).to.exist;
  });

  it("should return success method _checkSellIn", () => {
    expect(mockInstance._checkSellIn).to.exist;
  });
  describe("method return with success ", () => {
    it("should return method _checkSellIn => if sellIn === 0 return quality = 0", () => {
      let mockProduct = new Item("Backstage", -1, 5);
      let qualityResult = mockProduct.quality;
      if (mockProduct.sellIn < 0) qualityResult = 0;
      mockInstance._checkSellIn(mockProduct);
      expect(mockProduct.quality).to.equal(qualityResult);
    });

    it("check method _checkSellIn => if sellIn > 10 return quality++", () => {
      let mockBackstageItem = new Item("Backstage", 15, 5);
      let qualityResult = mockBackstageItem.quality;
      if (mockBackstageItem.sellIn > 10) qualityResult++;
      mockInstance._checkSellIn(mockBackstageItem);
      expect(mockBackstageItem.quality).to.equal(qualityResult);
    });
    it("should return method _checkSellIn => if sellIn < 10 return quality += 2", () => {
      let mockBackstageProduct = new Item("Backstage", 8, 5);
      let qualityResult = mockBackstageProduct.quality;
      if (mockBackstageProduct.sellIn < 10) qualityResult += 2;
      mockInstance._checkSellIn(mockBackstageProduct);
      expect(mockBackstageProduct.quality).to.equal(qualityResult);
    });

    it("should return method _checkSellIn => if sellIn < 5 return quality += 3", () => {
      let mockBackstage = new Item("Backstage", 4, 5);
      let qualityResult = mockBackstage.quality;
      if (mockBackstage.sellIn < 5) qualityResult += 3;
      mockInstance._checkSellIn(mockBackstage);
      expect(mockBackstage.quality).to.equal(qualityResult);
    });
  });
});
