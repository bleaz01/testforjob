const { expect } = require("chai");
const sinon = require("sinon");

const { Item, LegendaryProduct } = require("../src/gilded_rose.js");

describe("Class LegendaryProduct extend Product ", function () {
  // before(function () {
  const mockInstance = new LegendaryProduct();

  it("should inherited method checkQuality", () => {
    expect(mockInstance.checkQuality).to.exist;
  });
  it("should inherited method handleDegradeQualityProduct", () => {
    expect(mockInstance.handleDegradeQualityProduct).to.exist;
  });

  it("should inherited method handleQualityProduct", () => {
    expect(mockInstance.handleQualityProduct).to.exist;
  });

  it("should inherited method dayAfterDay", () => {
    expect(mockInstance.dayAfterDay).to.exist;
  });
  it.skip("check if max_quality = 80", () => {
    let propretyLegendary = { items: [], min_quality: 0, max_quality: 80 };
    expect(Object.values(propretyLegendary)).deep.equal(
      Object.values(mockInstance)
    );
  });
});
