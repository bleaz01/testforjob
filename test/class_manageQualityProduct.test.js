import { expect } from "chai";
const sinon = require("sinon");

const { Item, ManageQualityProduct } = require("../src/gilded_rose.js");

describe("Class ManageQualityProduct", function () {
  // before(function () {
  const mockInstance = new ManageQualityProduct();
  const ProductNotDownQualityMoke = [
    "Aged Brie",
    "Sulfuras, Hand of Ragnaros",
    "Backstage passes to a TAFKAL80ETC concert",
  ];

  it("should return success method checkQuality", () => {
    expect(mockInstance.checkQuality).to.exist;
  });

  it("should return success method handleDegradeQualityProduct", () => {
    expect(mockInstance.handleDegradeQualityProduct).to.exist;
  });

  it("should return success method handleQualityProduct", () => {
    expect(mockInstance.handleQualityProduct).to.exist;
  });

  it("should return success method dayAfterDay", () => {
    expect(mockInstance.dayAfterDay).to.exist;
  });
  describe("method return with success ", () => {
    it("should return method checkQuality() => quality > 50  = quality 50 ", () => {
      let mockProductFood = new Item("food", 5, 51);
      mockInstance.checkQuality(mockProductFood);
      expect(mockProductFood.quality).to.equal(50);
    });

    it("should  return method checkQuality() => quality < 0  = quality 0 ", () => {
      let mockIceProduct = new Item("ice", 5, -1);
      mockInstance.checkQuality(mockIceProduct);
      expect(mockIceProduct.quality).to.equal(0);
    });

    // it("should return method handleQualityProduct() => return quality++", () => {
    //   let mockAgedBrie = new Item("Aged Brie", 12, 10);
    //   let qualityUp = mockAgedBrie.quality;
    //   if (ProductNotDownQualityMoke.includes(mockAgedBrie.name)) {
    //     qualityUp++;
    //   }
    //   mockInstance.handleQualityProduct(mockAgedBrie),
    //     expect(mockAgedBrie.quality).to.equal(qualityUp);
    // });
    it("should return method handleDegradeQualityProduct() => return quality--", () => {
      let mockVegetableProduct = new Item("vegetable", 12, 10);
      let qualityDown = mockVegetableProduct.quality;
      if (!ProductNotDownQualityMoke.includes(mockVegetableProduct.name))
        qualityDown--;
      mockInstance.handleDegradeQualityProduct(mockVegetableProduct),
        expect(mockVegetableProduct.quality).to.equal(qualityDown);
    });

    it("should return method handleDegradeQualityProduct() => quality -= 2", () => {
      let mockMilkProduct = new Item("milk", -5, 5);
      let qualityResult = mockMilkProduct.quality;
      if (mockMilkProduct.sellIn < 0) qualityResult -= 2;
      mockInstance.handleDegradeQualityProduct(mockMilkProduct);
      expect(mockMilkProduct.quality).to.equal(qualityResult);
    });
  });
  //});
});
