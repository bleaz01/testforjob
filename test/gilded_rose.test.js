const { expect } = require("chai");
const {
  ManageQualityProduct,
  Shop,
  Item,
  AgedBrieProduct,
  BackstageProduct,
  LegendaryProduct,
  ConjuredProduct,
} = require("../src/gilded_rose.js");

describe("Gilded Rose", function () {
  it.skip("Check if Once the sell by date has passed, Quality degrades twice as fast", function () {
    const producOne = new Shop([new Item("foo", -5, 25)]);
    producOne.updateQuality();
    const item = producOne.items;
    expect(item[0].quality).to.equal(23);
  });

  it.skip("Check if the Quality of product is never more than 50", function () {
    const neverUpfifty = new Shop([new Item("Aged Brie", 2, 50)]);
    neverUpfifty.updateQuality();
    const item = neverUpfifty.items;
    expect(item[0].quality).to.not.equal(51);
  });

  it.skip('Check if "Sulfuras legendary product", never has to be sold or decreases in Quality', function () {
    const legendary = new Shop([new Item("Sulfuras, Hand of Ragnaros", 0, 80)]);
    legendary.updateQuality();
    const item = legendary.items;
    expect(item[0].quality).to.equal(80);
    expect(item[0].sellIn).to.equal(0);
  });

  it.skip('Check if "Backstage passes", Quality increases by 2 when there are SellIn < 10', function () {
    const backstageIncreasesTwo = new Shop([
      new Item("Backstage passes to a TAFKAL80ETC concert", 7, 32),
    ]);
    backstageIncreasesTwo.updateQuality();
    const item = backstageIncreasesTwo.items;
    expect(item[0].quality).to.equal(34);
  });

  it.skip('Check if "Backstage passes", Quality increases by 3 when there are SellIn < 5', function () {
    const backstageIncreasesThree = new Shop([
      new Item("Backstage passes to a TAFKAL80ETC concert", 3, 30),
    ]);
    backstageIncreasesThree.updateQuality();
    const item = backstageIncreasesThree.items;
    expect(item[0].quality).to.equal(33);
  });

  it.skip('Check if "Backstage passes", Quality = 0 when there are SellIn < 0', function () {
    const backstageQualityZero = new Shop([
      new Item("Backstage passes to a TAFKAL80ETC concert", 0, 30),
    ]);
    backstageQualityZero.updateQuality();
    const item = backstageQualityZero.items;

    expect(item[0].quality).to.equal(0);
  });

  it.skip('Should "Conjured" items degrade in Quality twice as fast as normal product', function () {
    const conjured = new Shop([new Item("Conjured Mana Cake", 10, 29)]);
    conjured.updateQuality();
    const item = conjured.items;
    expect(item[0].quality).to.equal(27);
  });

  it.skip('Should "Conjured" Once the sell by date has passed, Quality degrades twice as fast', function () {
    const conjuredDegrades = new Shop([new Item("Conjured Mana Cake", -2, 40)]);
    conjuredDegrades.updateQuality();
    const item = conjuredDegrades.items;
    expect(item[0].quality).to.equal(36);
  });
  describe("Change methode updateQuality to static", () => {
    it("Should the degraded quality day after day ", function () {
      const mockItem = [new Item("foo", 2, 5)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(4);
    });

    it("Should check the Quality of an item is never negative", function () {
      const mockItem = [new Item("foo", 2, 0)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.not.equal(-1);
    });

    it("Should check Once the sell by date has passed, Quality degrades twice as fast", function () {
      const mockItem = [new Item("foo", -5, 25)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(23);
    });

    it("Should check the Quality of produc is never more than 50", function () {
      const mockItem = [new Item("Aged Brie", 2, 50)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.not.equal(51);
    });

    it('Should check "Sulfuras legendary produc", never has to be sold or decreases in Quality', function () {
      const mockItem = [new Item("Sulfuras, Hand of Ragnaros", 0, 80)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(80);
      expect(mockItem[0].sellIn).to.equal(0);
    });

    it('Should check "Backstage passes", Quality increases by 2 when there are SellIn < 10', function () {
      const mockItem = [
        new Item("Backstage passes to a TAFKAL80ETC concert", 7, 32),
      ];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(34);
    });

    it('Should check "Backstage passes", Quality increases by 3 when there are SellIn < 5', function () {
      const mockItem = [
        new Item("Backstage passes to a TAFKAL80ETC concert", 3, 30),
      ];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(33);
    });

    it('Should check "Backstage passes", Quality = 0 when there are SellIn < 0', function () {
      const mockItem = [
        new Item("Backstage passes to a TAFKAL80ETC concert", 0, 30),
      ];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(0);
    });

    it('Should check "Conjured" items degrade in Quality twice as fast as normal produc', function () {
      const mockItem = [new Item("Conjured Mana Cake", 10, 29)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(27);
    });

    it('Should check "Conjured" Once the sell by date has passed, Quality degrades twice as fast', function () {
      const mockItem = [new Item("Conjured Mana Cake", -2, 40)];
      Shop.updateQuality(mockItem);
      expect(mockItem[0].quality).to.equal(36);
    });
  });

  describe("Global test", function () {
    it("check constructor Item param (sellIn, quality, name)", function () {
      let ItemParams = { name: "", sellIn: "", quality: "" };
      const product = new Item();
      expect(Object.keys(product)).deep.equal(Object.keys(ItemParams));
    });

    it.skip("check constructor Shop param (item[])", function () {
      const ShopParams = { items: [] };
      const product = new Shop();
      expect(Object.keys(product)).deep.equal(Object.keys(ShopParams));
    });

    it("should return success static methode updateQuality", function () {
      expect(Shop.updateQuality).to.exist;
    });

    it("should create class ManageQualityProduct", function () {
      const mockManageQualityProduct = new ManageQualityProduct();
      expect(mockManageQualityProduct).to.exist;
    });

    it("should create class AgedBrieProduct", function () {
      const mockAgedBrieProduct = new AgedBrieProduct();
      expect(mockAgedBrieProduct).to.exist;
    });

    it("should create class BackstageProduct", function () {
      const mockBackstageProduct = new BackstageProduct();
      expect(mockBackstageProduct).to.exist;
    });

    it("should create class ProducConjured", function () {
      const mockConjuredProduct = new ConjuredProduct();
      expect(mockConjuredProduct).to.exist;
    });

    it("should create class ProducLegend", function () {
      const mockLegendaryProduct = new LegendaryProduct();
      expect(mockLegendaryProduct).to.exist;
    });

    it("should create Abstract class Shop", function () {
      expect(() => new Shop()).to.throw(
        "Abstract class Shop can't be instantiated."
      );
    });

    it("should create method to you need implemented", function () {
      expect(() => Shop.prototype.dayAfterDay()).to.throw(
        "Method 'dayAfterDay()' must be implemented."
      );
    });
  });
});
