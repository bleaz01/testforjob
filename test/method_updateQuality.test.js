const { expect, assert } = require("chai");
const sinon = require("sinon");

const {
  ManageQualityProduct,
  Shop,
  Item,
  AgedBrieProduct,
  BackstageProduct,
  ConjuredProduct,
  LegendaryProduct,
} = require("../src/gilded_rose.js");

describe("UpdateQuality call class", function () {
  const specialProduct = [
    "Sulfuras, Hand of Ragnaros",
    "Aged Brie",
    "Backstage passes to a TAFKAL80ETC concert",
    "Conjured Mana Cake",
  ];

  it("should call class ManageQualityProduct", () => {
    let product = new Item("Elixir of the Mongoose", -2, 20);
    let productCompare = [new Item("Elixir of the Mongoose", -2, 20)];
    if (!specialProduct.includes(product.name)) {
      new ManageQualityProduct(product).dayAfterDay();
    }
    Shop.updateQuality(productCompare);
    expect(product.quality).to.equal(productCompare[0].quality);
  });

  it("should call class Brie", () => {
    let brie = new Item("Aged Brie", 2, 20);

    let brieCompare = [new Item("Aged Brie", 2, 20)];
    if (brie.name === "Aged Brie") {
      new AgedBrieProduct(brie).dayAfterDay();
    }
    Shop.updateQuality(brieCompare);
    expect(brie.quality).to.equal(brieCompare[0].quality);
  });

  it("should call class BackstageProduct", () => {
    let backstage = new Item(
      "Backstage passes to a TAFKAL80ETC concert",
      2,
      20
    );
    let backstageCompare = [
      new Item("Backstage passes to a TAFKAL80ETC concert", 2, 20),
    ];
    if (backstage.name === "Backstage passes to a TAFKAL80ETC concert") {
      new BackstageProduct(backstage).dayAfterDay();
    }
    Shop.updateQuality(backstageCompare);
    expect(backstage.quality).to.equal(backstageCompare[0].quality);
  });

  it("should call class ConjuredProduct", () => {
    let conjured = new Item("Conjured Mana Cake", 2, 20);
    let conjuredCompare = [new Item("Conjured Mana Cake", 2, 20)];
    if (conjured.name === "Conjured Mana Cake") {
      new ConjuredProduct(conjured).dayAfterDay();
    }
    Shop.updateQuality(conjuredCompare);
    expect(conjured.quality).to.equal(conjuredCompare[0].quality);
  });

  it("should call class LegendaryProduc", () => {
    let legendary = new Item("Sulfuras, Hand of Ragnaros", -1, 80);
    let legendaryCompare = [new Item("Sulfuras, Hand of Ragnaros", -1, 80)];
    if (legendary.name === "Sulfuras, Hand of Ragnaros") {
      new LegendaryProduct(legendary).dayAfterDay();
    }
    Shop.updateQuality(legendaryCompare);
    expect(legendary.quality).to.equal(legendaryCompare[0].quality);
  });

  describe("handle new product", () => {
    it('should return new ConjuredProduct if the product name contains "Conjured"', () => {
      const containsConjuredName = [new Item("Conjured banane", 5, 3)];
      Shop.updateQuality(containsConjuredName);
      expect(containsConjuredName[0].quality).to.equal(1);
    });

    it('should return new LegendaryProuduct if the product name contains "Sulfuras"', () => {
      const containeSulfurasName = [new Item("Sulfuras, ring", 5, 81)];

      Shop.updateQuality(containeSulfurasName);
      expect(containeSulfurasName[0].quality).to.equal(80);
    });

    it('should return new BackstageProduct if the product name contains "Backstage passes"', () => {
      let containBackstageName = [
        new Item("Backstage passes for Nirvana", 5, 3),
      ];
      Shop.updateQuality(containBackstageName);
      expect(containBackstageName[0].quality).to.equal(6);
    });
  });
});
